package com.example.myfirstapp;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;


public class MainActivity extends Activity {
	
    public final static String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
    public Context contextNew;
    private MySQLiteHelper db;
    MyAdapter adapter;
    ArrayList<Item> books;
        
    protected void onResume(Bundle savedInstanceState) {
    	this.adapter.notifyDataSetChanged();
    }
    
    protected void initList(){
    	// 1. pass context and data to the custom adapter
    	this.books = db.getAllBooks();
        this.adapter = new MyAdapter(this, this.books);
        Log.d("a", "2");
        // 2. Get ListView from activity_main.xml
        ListView listView = (ListView) findViewById(R.id.listview);
        listView.setClickable(true);
        
        listView.setOnItemClickListener(
        		
        		new AdapterView.OnItemClickListener() {  
			   


					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub
						displayClick(arg2);
						
					}  
        			}
        		
        		);
        
        
        
        
        // 3. setListAdapter
        listView.setAdapter(this.adapter);
        
        Log.d("a", "3");
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        // then you use
        
        
        
       // Toast.makeText(this, prefs.getString("example_text", "emptyy"), Toast.LENGTH_LONG).show();
        
       // Preference myPref = (Preference) findPreference("example_text");
     //   Toast.makeText(this, "Your welcome!", Toast.LENGTH_LONG).show();  
      //  Toast.makeText(this, findPreference(""), Toast.LENGTH_LONG).show(); 
        
        //SQLiteDatabase db = this.openOrCreateDatabase("db_test", MODE_PRIVATE, null);
           
        
     //   dbManage db = new dbManage(this); 
   //     SQLiteDatabase asd = db.getReadableDatabase();
        
        
        /**
         * CRUD Operations
         * */
        // add Books
        //db.addBook(new Book("Android Application Development Cookbook", "Wei Meng Lee"));  
        //db.addBook(new Book("Android Programming: The Big Nerd Ranch Guide", "Bill Phillips and Brian Hardy"));      
        //db.addBook(new Book("Learn Android App Development", "Wallace Jackson"));
 
        // get all books
        ArrayList<Item> list = db.getAllBooks();
 
        // delete one book
        //db.deleteBook(list.get(0));
 
        // get all books
        //db.getAllBooks();
        
        
        
        //Toast.makeText(this, String.valueOf(list.size()) , Toast.LENGTH_LONG).show();
    }
    
    protected void displayClick (int id) {
    	
    	//books.get(id).toString();
    	
    	//Item asd = findId(2,books);
    	Item dasd = books.get(id);    	
    	//Toast.makeText(this, dasd.getTitle() , Toast.LENGTH_LONG).show();
    	
    	Intent intent = new Intent(this, DisplayMessageActivity.class);
    	//EditText editText = (EditText) findViewById(R.id.edit_message);
    	//String message = editText.getText().toString();
    	intent.putExtra(EXTRA_MESSAGE, dasd.getTitle());
        startActivity(intent);
    }
    
    protected void displayClick (String str) {
    	Toast.makeText(this, str , Toast.LENGTH_LONG).show();
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	
    	
    	this.db = new MySQLiteHelper(this);
    	
    	
    	Log.d("a", "1");

        super.onCreate(savedInstanceState);
        
        
        setContentView(R.layout.activity_main);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        
        initList();
        
        
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
              
        return super.onCreateOptionsMenu(menu);

    }
    
    /** Called when the user clicks the Send button */
    public void sendMessage(View view) {
        // Do something in response to button
    	
    	//Intent intent = new Intent(this, DisplayMessageActivity.class);
    	//EditText editText = (EditText) findViewById(R.id.edit_message);
    	//String message = editText.getText().toString();
    	//intent.putExtra(EXTRA_MESSAGE, message);
        //startActivity(intent);
    	
    	EditText editText = (EditText) findViewById(R.id.edit_message);
    	String message = editText.getText().toString();

    	db.addBook(new Book(message, "unchanged"));
    	
    	initList();
    	//this.onCreate(this);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_search:
//                openSearch();
                return true;
            case R.id.action_settings:
            	Intent intent = new Intent(this, SettingsActivity.class);
            	startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
}
