package com.example.myfirstapp;

public class Book {
	 
    private int id;
    private String title;
    private String author;
 
    public Book(){}
 
    public Book(String title, String author) {
        super();
        this.title = title;
        this.author = author;
    }
    
    public String getTitle () {
    	return this.title;
    }
    
    public String getAuthor () {
    	return this.author;
    }
    
    public int getId () {
    	return this.id;
    }
    
    public void setAuthor (String a) {
    	this.author = a;
    }
    
    public void setTitle (String a) {
    	this.title = a;
    }
    
    public void setId (int a) {
    	this.id = a;
    }
 
    //getters & setters
 
    @Override
    public String toString() {
        return "Book [id=" + id + ", title=" + title + ", author=" + author
                + "]";
    }
}
