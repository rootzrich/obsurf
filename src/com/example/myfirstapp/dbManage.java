package com.example.myfirstapp;

import android.content.Context;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class dbManage extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "test_db2";
	private static final String TABLE_ID = "tid";
	private static final String TABLE_URL = "url";
	private static final String TABLE_HASH = "hash";
	private static final String TABLE_INTERVAL = "interval";
    private static final int DATABASE_VERSION = 2;
    private static final String DICTIONARY_TABLE_NAME = "dictionary";
    private static final String DICTIONARY_TABLE_CREATE =
                "CREATE TABLE " + DICTIONARY_TABLE_NAME + " (" +
                		TABLE_ID + " TEXT, " +
                		TABLE_URL + " TEXT, " +
                		TABLE_HASH + " TEXT, " +
                		TABLE_INTERVAL + " TEXT);";
    
    //private SQLiteDatabase db;
    
    dbManage(Context context) {
       super(context, DATABASE_NAME, null, DATABASE_VERSION);
        
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DICTIONARY_TABLE_CREATE);
    }
    
//    public int numRows() {
//    	
//    	SQLiteDatabase db = this.getWritableDatabase();
//    	
//    	final String DATABASE_COMPARE = "select count(*) from "+DICTIONARY_TABLE_NAME;
//
//    	int sometotal = (int) DatabaseUtils.longForQuery( db , DATABASE_COMPARE, null);  	
//    	
//    	return sometotal;
//    }
    
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older books table if existed
        db.execSQL("DROP TABLE IF EXISTS books");
 
        // create fresh books table
        this.onCreate(db);
    }
    
}