package com.example.myfirstapp;

public class Item {
	 
    private String title;
    private String description;
    private int id;
 
    public Item(String title, String description, int id) {
        super();
        this.title = title;
        this.description = description;
        this.id = id;
    }

	public Item() {
		// TODO Auto-generated constructor stub
	}

	public String getTitle() {
		// TODO Auto-generated method stub
		return this.title;
	}
	
	public String getDescription() {
		// TODO Auto-generated method stub
		return this.description;
	}
    
    
    // getters and setters...  
}